# Setting Up Hadoop Environment

## What is Big Data ?

Bernard Marr, defines Big Data as the digital trace that we are generating in this digital era. The Digital trace is made up of all  the data that is captured when we use digital technology.

Ernst Young, refers Big Data to the dynamic large and disparate volume of data being created by people,tools and machines.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/big_data_1.png?inline=false)


![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/big_data_2.jpg?inline=false)


## V’s of Big Data

* **Velocity**->  Speed of data at which data accumulates.
* **Volume**->    Scale of data or increase in amount of data store.
* **Variety**->   Diversity of data means Structured, Unstructured.
* **Veracity**-> Conformity to facts and accuracy.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/vs_bgdata.png?inline=false)

## Characteristics of Big Data

1. Recommendation Engines are a common application of big data.
2. Companies like Amazon, Netflix use the Algorithm of Big Data.
3. SIRI personal assistants of Apple devices use Big Data to answer the infinite number of  questions.
4. Google makes recommendations based on Big Data.


## What is Hadoop

The Apache Hadoop software library is a framework that allows for the distributed processing of large data sets across clusters of computers using simple programming models. It is designed to scale up from single servers to thousands of machines, each offering local computation and storage. Rather than rely on hardware to deliver high-availability, the library itself is designed to detect and handle failures at the application layer, so delivering a highly-available service on top of a cluster of computers, each of which may be prone to failures.

## Features of Hadoop

* Hadoop is Open Source
* Hadoop cluster is Highly Scalable
* Hadoop provides Fault Tolerance
* Hadoop provides High Availability
* Hadoop is very Cost-Effective
* Hadoop is Faster in Data Processing
* Hadoop is based on the Data Locality concept
* Hadoop provides Feasibility

# KO1: Able to install Hadoop

1.Hadoop is running using Java so first download java if it is not installed using the below link.

[Download Java ](https://www.oracle.com/in/java/technologies/javase/javase-jdk8-downloads.html)

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/java_download.png?inline=false)

2.Download Hadoop from apache.org

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/hadoop_dw.png?inline=false)

3.Check the localhost connection .

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/SSh.png?inline=false)

4.Unzip the downloaded Hadoop file. The Unzipped folder contains many subfolders.Make the changes in the **etc** folder.

5.Open the file **core-site.xml**

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-06-11.png?inline=false)

### Add two properties in this file.
* temporary directory
* default file system

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-06-33.png?inline=false)

7.Now open the file **hdfs-site.xml**. 

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-07-24.png?inline=false)

### Add three properties in this file.
* datanode
* namenode
* duplicated values

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-07-33.png?inline=false)

8.Now open the file **mapred-site.xml**. 

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-08-33.png?inline=false)

### Add one properties in this file.
* yarnframework

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-08-41.png?inline=false)

9.Now open the file **yarn-site.xml**. 

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-09-48.png?inline=false)

### Add five properties in this file.
* services
* class
* hostname
* enable acl
* add whitelist

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-10-03.png?inline=false)

10.Start the **dfs** services

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-16-29.png?inline=false)

11.Start the **yarn** services

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-21-17.png?inline=false)

12.Check that all the services of dfs and yarn is started by running **jps** command.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-13_16-22-05.png?inline=false)

* All the services has started.Hadoop is successfully installed we are good to go further.

# KO2: Able to store data in HDFS

## What is **HDFS**

Hadoop Distributed file system is used to store and retrieve files.
* HDFS is built on commodity hardware
* Highly fault tolerant,hardware failure is the norm.
* Suited to Batch processing data access has high throughput rather than low latency.
* Supports very large data sets.

## Master And Slave Nodes

There are many nodes and hdfs choose among all a **Master** and all other **Slave**. Namenode is the master node which contains 
all metadata of all other slave nodes. Datanode is the node which physically stores the data.

## Storing File in HDFS

If there is a very large file say in Petabytes then file will split into blocks and these blocks are stored at different nodes.
The size of each block is same 128 MB.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__11_.png?inline=false)

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__12_.png?inline=false)

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__14_.png?inline=false)

## Step-1) Make the directory

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-15_16-15-11.png?inline=false)

## step-2) copy file from local to new directory

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-15_16-33-11.png?inline=false)

### Another way to copy file

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-15_16-36-04.png?inline=false)

## step-3) copy one file into new file

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-15_16-39-12.png?inline=false)

## step-4) Get file from **HDFS**

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot_from_2021-05-15_16-45-02.png?inline=false)

## Fault Tolerance of Datanode

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__15_.png?inline=false)

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__17_.png?inline=false)

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__16_.png?inline=false)

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__18_.png?inline=false)

## Fault Tolerance of Namenode

Namenode is the heart of the HDFS.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__19_.png?inline=false)

1. Metadata

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__21_.png?inline=false)

2. Secondary Namenode

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__22_.png?inline=false)

# KO3: Able to Process data with MapReduce

## 1.What is MapReduce

* It is a programming paradigm that follows Distributed Programming Approach.
* In today's world a lot of data is generated every minute. so, mapreduce process data in two phases.

1. **Map**
Runs on multiple nodes of the clusters.
So there are multiple Map.

2. **Reduce**
Takes the output of the map and further process it
to give final result.

So, for developer there are only two operations to be performed.
Rest of the work is done by the Hadooop behind the scenes.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__24_.png?inline=false)

**Map** produces the output in the Key-Value and **Reduce** collects all Key-Value pairs from Map and produces result in a 
Meaningful way.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__25_.png?inline=false)

## 2.Data Flow in MapReduce

* MapReduce count the number of words from the file in petabytes.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__26_.png?inline=false)

* In Map flow each row emits {Key,Value} pairs.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__27_.png?inline=false)

* In Reduce flow it produces the meaningful result.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__28_.png?inline=false)


![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__30_.png?inline=false)

## 3.Implement MapReduce in java

* It uses the Map and Reduce class where logic is implemented.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__31_.png?inline=false)

* Map Step

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__32_.png?inline=false)

* Setting up the Job

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__33_.png?inline=false)

## 4.Set up the Map Reduce

*Create the Map class

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__34_.png?inline=false)

*Create the Reduce class

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__35_.png?inline=false)

*Create the Main class

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__36_.png?inline=false)

# KO4: Able to Schedule and Manage data with MapReduce

## YARN
In 2013, hadoop architecture was changed and MapReduce was divided into two MapReduce AND Yarn.
* MapReduce used for what operatons we want to define.
* Yarn was responsible how these operations run across cluster.

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__38_.png?inline=false)

It has two components :
1. Resource Manager
2. Node Manager

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__37_.png?inline=false)

## There are Three Scheduling Policies

1. FIFO
2. Capacity
3. Fair

** FIFO Scheduler**

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__40_.png?inline=false)

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__41_.png?inline=false)

** Capacity Scheduler**

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__44_.png?inline=false)

** Fair Scheduler

![](https://gitlab.com/abhijain7417/apache-hadoop/-/raw/master/Screenshot__46_.png?inline=false)








